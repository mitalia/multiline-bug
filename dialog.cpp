#include "dialog.h"
#include <QComboBox>
#include <QGridLayout>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent)
{
    this->setMinimumSize(0,0);
    QGridLayout *l=new QGridLayout(this);
    QComboBox *gb=new QComboBox(this);
    gb->addItem("foo\nbar\nbaz\nqux\nquux\ncorge\ngrault\ngarply\nwaldo\nfred\nplugh\nxyzzy\nthud");
    l->addWidget(gb,1,1);
    l->addItem(new QSpacerItem(100,100, QSizePolicy::MinimumExpanding), 1, 2);
    l->addItem(new QSpacerItem(100,100, QSizePolicy::MinimumExpanding), 2, 1);
}
