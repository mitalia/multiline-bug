#-------------------------------------------------
#
# Project created by QtCreator 2014-05-31T19:48:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = multiline-bug
TEMPLATE = app


SOURCES += main.cpp \
    dialog.cpp

HEADERS  += \
    dialog.h

FORMS    +=
